" $ mkdir -p ~/.vim/bundle
" " $ cd ./vim/bundle
" " $ git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
" " $ vi ~/.vimrc

set nocompatible               " be iMproved
filetype off                   " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
" required!
Bundle 'gmarik/vundle'

" My Bundles here:
"
" original repos on github
Bundle 'tpope/vim-fugitive'
Bundle 'Lokaltog/vim-easymotion'
Bundle 'rstacruz/sparkup', {'rtp': 'vim/'}
Bundle 'tpope/vim-rails.git'
" vim-scripts repos
Bundle 'L9'
Bundle 'FuzzyFinder'
Bundle 'Align'
Bundle 'taglist.vim'
"Bundle 'Source-Explorer-srcexpl.vim'
Bundle 'http://github.com/vim-scripts/SrcExpl'
Bundle 'The-NERD-tree'
Bundle 'trinity.vim'
Bundle 'AutoComplPop'
Bundle 'bufexplorer.zip'

" non github repos
Bundle 'git://git.wincent.com/command-t.git'
" ...

filetype plugin indent on     " required!
"
" Brief help
" :BundleList          - list configured bundles
" :BundleInstall(!)    - install(update) bundles
" :BundleSearch(!) foo - search(or refresh cache first) for foo
" :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Bundle command are not allowed..


" nowdac's KeyMap

"map <F1> K
"map <F2> :%s/\s\+$//ge<CR>:w!<CR>
"map <F3> v]}zf
"map <F4> zo
"map <F5> :25vs .<cr>:set nonumber<CRw
"map <F6>
"map <F7> O
"map <F8> [i
"map <F9> gd
"map <F10> ''
"map <F9> :cs kill -1<CR>:!mkcscope<CR>:cs add cscope.out<CR>
"map <F11> 
"map <F12>

map <F2> :BufExplorer<CR>
map <F3> :Tlist<CR>
map <F4> :NERDTreeToggle<CR>
map <F5> :SrcExplToggle<CR>
map <F7> :vs $MYVIMRC<CR>
map <F8> :source $MYVIMRC<CR>
map <F9> :cs kill -1<CR>:!mkcscope.sh<CR>:cs add cscope.out<CR>

nmap , :e %:p:s,.h$,.X123X,:s,.c$,.h,:s,.X123X$,.c,<CR>
map cm <esc>:make<cr>
map co <esc>:copen<cr>
map cn <esc>:cn<cr>
map cp <esc>:cp<cr>
"nmap <C-@>1 :BufExplorer<CR>
"nmap <S-F2>:NERDTreeToggle<CR>
"nmap <S-F3>:SrcExplToggle<CR>
"nmap <S-F4>:Tlist<CR>
"nmap <S-F11>:vs $MYVIMRC<CR>
"nmap <S-F12>:source $MYVIMRC<CR>

filetype on
syntax on
colorscheme torte
set nu
set hls is

set autoindent	"자동들여쓰기"
set cindent	"C자동들여쓰기"
set smartindent "스마트자동들여쓰기"
set nowrap	"자동줄바꿈 x"
set nowrapscan	"찾기에서 파일 맨 끝일 경우 계속 찾지 않음"
set ignorecase	"찾기 대/소문자 구별 않음"
set incsearch	"점진적 찾기"
set ruler	"상태표시줄 커서위치 보여줌"
set tabstop=4 	"간격"
set shiftwidth=4"자동 들여쓰기 간격"
set showcmd	"부분적인 명령어를 상태라인에 보여줌"
set showmatch	"매치되는 괄호의 반대쪽을 보여줌"
set autowrite	":next :make 같은 명령을 입력하면 자동으로 저장"
set linespace=3	"줄간격"
set title 	"타이틀바에 현재 편집중인 파일 표시"
set expandtab "탭을 스페이스로 치환"
set cursorline
set backspace=2 "삽입 모드에서 백스페이스를 계속허용"
"set visualbell "비프음 대신 화면 번쩍이는 효과"

"자동 괄호 채우기
" imap { {<CR><CR>}<ESC>kkA<CR>
"공백문자를 표시할 기호를 설정
" set listchars=tab:\|\ ,trail:.,extends:>,precedes:<,eol:$
"공백문자를 표시
" set list

" Remove any trailing whitespace that is in the file
autocmd BufRead,BufWrite * if ! &bin | silent! %s/\s\+$//ge | endif


" SrcExpl
nmap <C-H> <C-W>h
nmap <C-J> <C-W>j
nmap <C-K> <C-W>k
nmap <C-L> <C-W>l
nmap <C-I> <C-W>j:call g:SrcExpl_Jump()<CR>
nmap <C-O> :call g:SrcExpl_GoBack()<CR>
let g:SrcExpl_pluginList = [
    \ "__Tag_List__",
    \ "NERD_tree_1",
    \ "Source_Explorer"
\ ]

" Nerd-TREE
let NERDTreeWinPos='right'

" ctags
set tags=..\tags

" cscope
if has("cscope")
    set csprg=cscope
    set cscopetag
    set csto=0
    set nocsverb

    if filereadable("cscope.out")
        cs add cscope.out
    elseif $CSCOPE_DB != ""
        cs add $CSCOPE_DB
    endif

    set csverb
    " nmap
    nmap <C-\>s :cs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>g :cs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>c :cs find c <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>t :cs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>e :cs find e <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
    nmap <C-\>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
    nmap <C-\>d :cs find d <C-R>=expand("<cword>")<CR><CR>


    " Using 'CTRL-spacebar' (intepreted as CTRL-@ by vim) then a search type
    " makes the vim window split horizontally, with search result displayed in
    " the new window.
    "
    " (Note: earlier versions of vim may not have the :scs command, but it
    " can be simulated roughly via:
    "    nmap <C-@>s <C-W><C-S> :cs find s <C-R>=expand("<cword>")<CR><CR>

    nmap <C-@>s :scs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@>g :scs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@>c :scs find c <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@>t :scs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@>e :scs find e <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@>f :scs find f <C-R>=expand("<cfile>")<CR><CR>
    nmap <C-@>i :scs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
    nmap <C-@>d :scs find d <C-R>=expand("<cword>")<CR><CR>


    " Hitting CTRL-space *twice* before the search type does a vertical
    " split instead of a horizontal one (vim 6 and up only)
    "
    " (Note: you may wish to put a 'set splitright' in your .vimrc
    " if you prefer the new window on the right instead of the left

    nmap <C-@><C-@>s :vert scs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>g :vert scs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>c :vert scs find c <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>t :vert scs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>e :vert scs find e <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>f :vert scs find f <C-R>=expand("<cfile>")<CR><CR>
    nmap <C-@><C-@>i :vert scs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
    nmap <C-@><C-@>d :vert scs find d <C-R>=expand("<cword>")<CR><CR>

endif

